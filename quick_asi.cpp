﻿#include <iostream>
#include <filesystem>
#include <Windows.h>
#include <TlHelp32.h>


uintptr_t get_process() {

	HANDLE process_handle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	
	PROCESSENTRY32 process_entry;
	process_entry.dwSize = sizeof(process_entry);

	do {
		if (!wcscmp(process_entry.szExeFile, L"gta_sa.exe")) {
			
			uintptr_t process_id = process_entry.th32ProcessID;
			CloseHandle(process_handle);

			return process_id;
		}
	} while (Process32Next(process_handle, &process_entry));
}


int main() {
	
	uintptr_t process = get_process();

	std::filesystem::path script_path = "asi";
	std::filesystem::path current_path = std::filesystem::current_path();
	

	for (const auto& entry : std::filesystem::directory_iterator(script_path)) {
		
		char full_path[MAX_PATH];
		sprintf_s(full_path, "%s\\%s", current_path.string().c_str(), entry.path().string().c_str());
		
		HMODULE module = GetModuleHandleA("kernel32.dll");

		void* load_library = (void*)GetProcAddress(module, "LoadLibraryA");
		
		HANDLE process_handle = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION, FALSE, process);
		LPVOID allocate = VirtualAllocEx(process_handle, NULL, sizeof(full_path), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
		
		WriteProcessMemory(process_handle, allocate, full_path, sizeof(full_path), NULL);
		CreateRemoteThread(process_handle, 0, 0, (LPTHREAD_START_ROUTINE)load_library, allocate, 0, 0);

		CloseHandle(process_handle);
	}
}